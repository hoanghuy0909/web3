<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/cssctsanpham/cssctsanpham.css">
</head>
<body>
	<h3 class="title">Thông Tin Sản Phẩm</h3>
	<div>
		<div class="img-sp">
			<img src="img/imgtrangchu/img_8134_thumb_400x600.jpg">
		</div>
		<div class="chitietsp">
			<h3>Áo sơ mi</h3>
			<p>Giá bán: <font color="red">300000</font></p>
			<p>Mô tả sản phẩm: </p>
			<p>Giao hàng và đổi trả hàng: Đổi hàng trong vòng 72h
               Giao hàng nội thành Hà Nội 20.000đ trong vòng 48h
               Giao hàng ngoài Hà Nội trong vòng 72h – Tính phí
               Thanh toán online hoặc thanh toán khi nhận hàng </p>
		</div>
		<div class="buy">
			<form>
				<input type="submit" name="btnmua" class="btn btn-success" value="Mua Hàng">
			</form>
		</div>
	</div>

</body>
</html>