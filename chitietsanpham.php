<!DOCTYPE html>
<html>
<head>
	<title>web bán hàng</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/giaodien.css">
</head>
<body>
	<div class="top">
		<div class="logotop">
			<?php include 'top/topphp.php'; ?>
		</div>
	</div>
	<hr>
	<div class="menu">
		<div class="mmenu">
			<?php include 'menu/menu.php'; ?>
		</div>
	</div>
	<hr>
	<div class="banner">
				<?php include "banner/banner.php"; ?>
		</div>
	</div>
	<div class="main">
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 mmain">
				<?php include 'chitietsanpham/ctsanphamphp.php'; ?>
			</div>
			<div class="col-sm-1"></div>
		</div>
	</div>
	<div class="footer">
		<?php include "footer/footer.php"; ?>
	</div>

</body>
</html>